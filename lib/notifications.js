const { promisify } = require('util');
const { IncomingWebhook } = require('@slack/client');
let context = 'Failure';

/**
 * Send message to slack.
 *
 * @param {string} slackHook - URL of the slack channel where notifications need to be send.
 * @param {object} attachments - attachment of the slack message with specified link.
 */
async function slackNotify(slackHook, attachments) {
    const text = '*OneCMS Builder - Error Handler*';
    const webhook = new IncomingWebhook(slackHook);
    const send = promisify(webhook.send).bind(webhook);

    try {
        await send({ text, attachments });
    } catch (error) {
        console.error('Slack notification failed.');
    }
}

/**
 * Notify user and oneCMS about the error.
 *
 * @param {string} error - Error message which is to be notified.
 */
async function notifyError(
    error,
    { buildUrl, slackHook }
) {
    const message = `[${context}] ${error.stack}`;

    let attachments = [
        {
            color: error.known ? 'warning' : 'danger',
            title: `ERROR: ${context}`,
            text: error.stack,
            fields: [
                {
                    title: 'buildUrl',
                    value: buildUrl
                }
            ]
        }
    ];

    if(error.log) {
        attachments[0].fields.push({
            title: 'log',
            value: error.log
        });
    }

    const params = {
        status: 'error',
        statusDescription: message,
        buildUrl: buildUrl,
        log: error.log || null
    };

    console.info('ERROR MESSAGE: ', message);

    await slackNotify(slackHook, attachments);
}

module.exports = { slackNotify, notifyError };