require('dotenv').config();
const assert = require('assert');

const {
  notifyError,
  slackNotify
} = require('./lib/notifications');

const slackHook = 'https://hooks.slack.com/services/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'; // FIXME: 

function missingParameter(paramName) {
  return `Mandatory parameter ${paramName} is not defined.`;
}

async function handleUpstreamJobLog() {
  const variableUpstreamJobLog = 'UPSTREAM_JOB_LOG';
  const variableUpstreamJobBuildUrl = 'UPSTREAM_BUILD_URL';

  console.log('printing all env variables visible from the main job: ', process.env);
  const upstreamJobLog = process.env[variableUpstreamJobLog] || '';
  const buildUrl = process.env[variableUpstreamJobBuildUrl] || '';

  assert(upstreamJobLog, missingParameter('upstreamJobLog'));
  assert(buildUrl, missingParameter('buildUrl'));

  if (upstreamJobLog) {
    console.log('upstream job log is: ', upstreamJobLog);

    // process log string: start
    const regex = /Finished:\sFAILURE/gm;
    const found = upstreamJobLog.match(regex);
    if (found !== null) {
      console.info('log has failure string, send notification to Slack.');
      const error = {
        stack: 'Upstream Jenkins build failed.',
        log: upstreamJobLog,
        known: true
      }
      await notifyError(error, {
        buildUrl,
        slackHook
    });
    } else {
      console.info('log has no failure string, do nothing?');
    }
    // process log string: end

  } else {
    console.error(`ERROR: could not get the ${variableUpstreamJobLog} variable contents.`);
  }
}

(() => {
  handleUpstreamJobLog();
})();